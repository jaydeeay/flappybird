using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace FlappyBird
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI score;

        public void UpdateScore()
        {
            if (score != null)
                score.text = GameManager.Instance.scoreCount.ToString();
        }

        public void Play()
        {
            SceneManager.LoadScene("Main");
        }

    }
}