using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlappyBird
{
    public class PipeSpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject pipe;        
        [SerializeField]
        private GameObject pipeNight;

        [SerializeField]
        private Transform spawnPoint;

        [Space, SerializeField, Range(-1, 1)]
        private float minHeight;
        [SerializeField, Range(-1, 1)]
        private float maxHeight;

        [Space, SerializeField]
        private float timeToSpawnFirstPipe;
        [SerializeField]
        private float timeToSpawnPipe;

        private Queue<GameObject> pipeList = new Queue<GameObject>();
        [SerializeField]private int pipeCount = 7;

        private void Start()
        {
            StartCoroutine(SpawnPipes());

            GameObject pipePrefab = pipe;
            if (GameManager.Instance.IsDay == false)
            {
                pipePrefab = pipeNight;
            }
            for (int i = 0; i < pipeCount; i++)
            {
                GameObject Object = Instantiate(pipePrefab);
                Object.SetActive(false);
                pipeList.Enqueue(Object);
            }
        }

        private Vector3 GetSpawnPosition()
        {
            return new Vector3(spawnPoint.position.x, Random.Range(minHeight, maxHeight), spawnPoint.position.z);
        }

        private IEnumerator SpawnPipes()
        {
            yield return new WaitForSeconds(timeToSpawnFirstPipe);

            StartCoroutine(SetPipe());

            do
            {
                yield return new WaitForSeconds(timeToSpawnPipe);

                StartCoroutine(SetPipe());
            } while (true);
        }

        private IEnumerator SetPipe()
        {
            GameObject Object = GetPoolObject();

            Object.transform.position = GetSpawnPosition();

            yield return new WaitForSeconds(7);

            Object.SetActive(false);
            pipeList.Enqueue(Object);
        }

        private GameObject GetPoolObject()
        {
            GameObject pipe = pipeList.Dequeue();
            pipe.SetActive(true);
            return pipe;
        }

        public void Stop()
        {
            StopAllCoroutines();
        }


    }
}