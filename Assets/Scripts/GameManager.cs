using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace FlappyBird
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private float timeToReloadScene;

        [Space, SerializeField]
        private UnityEvent onGameOver;
        [SerializeField]
        private UnityEvent onIncreaseScore;

        public bool IsDay = true;

        [SerializeField] private SpriteRenderer background;
        [SerializeField] private Sprite backgroundDay;
        [SerializeField] private Sprite backgroundNight;
 

        public bool isGameOver { get; private set; }
        public int scoreCount { get; private set; }

        public static GameManager Instance
        {
            get; private set;
        }

        private void Awake()
        {
            if (Instance != null)
                DestroyImmediate(gameObject);
            else
                Instance = this;

            if (Random.Range(0,2)==1)
                IsDay = true;
            else
                IsDay = false;
        }

        private void Start()
        {                        
            if (IsDay == false)
                background.sprite = backgroundNight;  
            else 
                background.sprite = backgroundDay;
        }

        public void GameOver()
        {
            Debug.Log("GameManager :: GameOver()");

            isGameOver = true;

            if (onGameOver != null)
                onGameOver.Invoke();

            StartCoroutine(ReloadScene());
        }

        public void IncreaseScore()
        {
            scoreCount++;

            if (onIncreaseScore != null)
                onIncreaseScore.Invoke();
        }
        private IEnumerator ReloadScene()
        {
            yield return new WaitForSeconds(timeToReloadScene);

            SceneManager.LoadScene(0);
        }
    }
}